#  **Uniformly partitioned Overlap-Add Convolution**


 ![Conv](res/UPOLA.png)
 Note. From "Partitioned convolution algorithms
for real-time auralization." by Frank Wefers, 2014, p. 106, Figure 5.1


# Setup
1. Install [Daisy Toolchain](https://github.com/electro-smith/DaisyToolchain).

2. Run [installer](oopsy-terrarium-support/install.sh) script. 

3. Drop impulse responses [here](IRLoader/irs). (48kHz IRs.)

4. Open [IRLoader](IRLoader/IRLoader.maxproj) patcher in Max.

5.  * Power on the Daisy.
    * Connect the Daisy via micro-USB.
    * Hold the "Boot" button and press the "Reset" button, 
    a single red LED should be lit.
    * Double click an IR to upload.


# Roadmap

* update chooser without having to restart Max
* preview IRs in Max before uploading
* truncate IRs
* test
* ?

## **Add support for different IR formats, sample rates...**

“Some software or hardware may need the files to be converted to a different format before they can be used. Other types of software/hardware may convert the files for you automatically when you load them. You’ll need to check the instructions for your particular hardware to find out which file formats are supported and what conversion may be necessary.” - https://www.celestionplus.com/


### Type
* Mono, Stereo, B-format Surround, ?

### Storage Format
  * .WAV, Proprietary, ?

### Sample Rate(kHz)
  * 44.1, 48.0, 88.2, 96.0, ?

Length (ms)
* 200, 500, ? 


# Resources

* [Digital signal processing for STM32 microcontrollers using CMSIS ](res/dsp-for-stm32-mcus-using-cmsis.pdf)
* [Digital Signal Processing using Arm Cortex-M based Microcontrollers](res/dsp-using-cortex-M-mcus.pdf)
* [Partitioned convolution algorithms for real-time auralization](res/partitioned-convolutuon-algorithms.pdf)
* [STM32H750 Datasheet](res/stm32h750-datasheet.pdf)

* Zölzer Udo. (2011). DAFX: Digital Audio effects. J. Wiley and Sons. 

* J. O. Smith, “Online Books,” Jos Home Page. [Online]. Available: https://ccrma.stanford.edu/~jos/. 

* Alan Oppenheim. RES.6-008 Digital Signal Processing. Spring 2011. Massachusetts Institute of Technology: MIT OpenCourseWare, https://ocw.mit.edu. License: Creative Commons BY-NC-SA. ⭐


