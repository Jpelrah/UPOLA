
#define OOPSY_TARGET_TERRARIUM (1)
#include "../genlib_daisy.h"
#include "../genlib_daisy.cpp"
#include "My_Effect.cpp"

struct App_My_Effect : public oopsy::App<App_My_Effect>
{

	float led1;
	float led2;

	void init(oopsy::GenDaisy &daisy)
	{
		daisy.gen = My_Effect::create(daisy.samplerate, daisy.blocksize);
		My_Effect::State &gen = *(My_Effect::State *)daisy.gen;
		daisy.param_count = 0;

		led1 = 0.f;
		led2 = 0.f;
	}

	void mainloopCallback(oopsy::GenDaisy &daisy, uint32_t t, uint32_t dt)
	{
		Daisy &hardware = daisy.hardware;
		My_Effect::State &gen = *(My_Effect::State *)daisy.gen;
	}

	void displayCallback(oopsy::GenDaisy &daisy, uint32_t t, uint32_t dt)
	{
		Daisy &hardware = daisy.hardware;
		My_Effect::State &gen = *(My_Effect::State *)daisy.gen;
	}

	void audioCallback(oopsy::GenDaisy &daisy, float **hardware_ins, float **hardware_outs, size_t size)
	{
		Daisy &hardware = daisy.hardware;
		My_Effect::State &gen = *(My_Effect::State *)daisy.gen;

		float *dsy_in1 = hardware_ins[0];
		float *dsy_in2 = hardware_ins[1];

		float *dsy_out1 = hardware_outs[0];
		float *dsy_out2 = hardware_outs[1];

		// in1:
		float *inputs[] = {dsy_in1};
		// out1:
		float *outputs[] = {dsy_out1};
		gen.perform(inputs, outputs, size);

		memcpy(dsy_out2, dsy_out1, sizeof(float) * size);
	}
};

// store apps in a union to re-use memory, since only one app is active at once:
union
{
	App_My_Effect app_My_Effect;
} apps;

oopsy::AppDef appdefs[] = {
	{"My_Effect", []() -> void
	 { oopsy::daisy.reset(apps.app_My_Effect); }},
};

int main(void)
{
	return oopsy::daisy.run(appdefs, 1, daisy::SaiHandle::Config::SampleRate::SAI_48KHZ);
}