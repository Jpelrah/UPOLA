# oopsy-terrarium-support
Variant of the oopsy package from Electro-smith developed by the Electric Guitar Innovation Lab (http://electricguitarinnovationlab.org/pedal) for use with the EGIL FX Testing Rig. 

## Requirements:
- Download and install Max 8 or later
  - http://cycling74.com 
- Install this custom Oopsy Package
  - Unzip and copy the ''oopsy'' folder here: 
    - <code>C:\Users\YourUserName\Documents\Max 8\Packages</code> on Windows
    - <code>~/Documents/Max 8/Packages/</code> on Mac OS X


## Exporting Code to Daisy Hardware
### Setup your computer to compile to the Daisy Hardware

- Install the Daisy Toolchain for 
  - Windows
  - https://github.com/electro-smith/DaisyWiki/wiki/1c.-Installing-the-Toolchain-on-Windows
  - Mac
    - https://github.com/electro-smith/DaisyWiki/wiki/1b.-Installing-the-Toolchain-on-Mac
	- when the install is complete, you can close Terminal with it prompting you to wait
- Install [http://cycling74.com Max 8 or later]
- Download or clone the Oopsy Package in this repo
  - Unzip and copy the ''oopsy'' folder here: 
    - <code>C:\Users\YourUserName\Documents\Max 8\Packages</code> on Windows
    - <code>~/Documents/Max 8/Packages/</code> on Mac OS X

### Loading Gen Patches on the Daisy 
Once all of the above steps are completed, you can compile code to the Daisy hardware directly from the FX Testing Rig software (in Max) with these steps:
- Plug the power supply into the Daisy-based pedal
- Connect the Daisy to your computer via micro-USB (led will glow red)
- On the Daisy microcontroller itself are two small cream-colored buttons labeled "Boot" and "Reset"
  - Hold the "Boot" button down while you press and release the "Reset" button
  - When you release the "Boot" button, the Daisy will be in DFU mode and will be capable of receiving your gen patch as new firmware
- Open the EGIL FX Testing Rig Project in Max (''FX Testing Rig.maxproj'')
  - The contents of your gen patch will be automatically pushed to the device when 1) the patch opens, 2) the patch is saved, or 3) the "Export Gen to C++" button is pressed
   - Put the Daisy in DFU mode whenever you want to update the firmware with new gen code
   - The C++ code is also exported to the folder <code>FX Testing Rig⁩>⁨patchers⁩</code> and contains 1) a .h file, 2) a .cpp file, and 3) a folder labeled gen_dsp
<br>
Consult https://github.com/electro-smith/DaisyWiki/wiki/1e.-Getting-Started-With-Oopsy-(Gen~-Integration)  for troubleshooting.
