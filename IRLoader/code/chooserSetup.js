
// setup chooser
function loadbang() {

    // get reference to chooser and clear contents
    const chooser = this.patcher.getnamed('IRChooser')
    chooser.clear()

    // construct path to irs
    const cwd = this.patcher.filepath
    const IRwd = cwd.split("/patchers/")[0]

    // get folder contents
    f = new Folder(IRwd + "/irs")

    // appened irs to chooser
    while (!f.end) {
        if (f.filename) {
            chooser.message("append", f.filename)
        }
        f.next()
    }
}


function bang(){
    loadbang
}


