const maxApi = require('max-api');
const fs = require('fs');
const wav = require('node-wav');
const {execSync} = require('child_process')
const {platform} = require('process');


// fires when user double clicks an item in chooser
maxApi.addHandler('filename', async (filename) => {

// read selected wav file
  fs.readFile(`../irs/${filename}`, (err, data) => {
    if (err) {
      return console.log(err);
    }

    // decode file and get data from first channel
    // need to support both channels and stero???
    const channelDataArray = wav.decode(data).channelData;
    const dataLen = channelDataArray[0].length;

    // complex data from real 
    var cmplxIR = []
    for (var i = 0; i < dataLen; i++) {
      cmplxIR[2 * i] = channelDataArray[0][i];
      cmplxIR[2 * i + 1] = 0.0;
    }

    const ir_template = `
      #pragma once
      #ifdef USE_ARM_DSP
      #include "arm_const_structs.h"
      #include "arm_math.h"
      #include "../libdaisy/src/util/ringbuffer.h"
      #endif
      
      #define BLOCK_SIZE 64
      #define IR_SIZE ${dataLen}
      #define FFT_SIZE 128
        
      constexpr int OVERLAP = BLOCK_SIZE - 1;
      constexpr int NEW_BLOCK_SIZE = 2 * BLOCK_SIZE - 1;
      constexpr int NUM_SUBFILTERS = ceil((float)IR_SIZE / BLOCK_SIZE);
        
      /**
       * @brief Uniformly partitioned Overlap-Add Convolution, ARM CMSIS DSP
       * @author Jacob Pelrah
       * @date December 25, 2021
       */
        
       #if (defined(USE_ARM_DSP) && defined(__arm__))
       class Convolver
       {
         
       public:
         /* fdl slot */
         typedef struct slot
         {
          float32_t spectra[2 * FFT_SIZE];
         } slot;
         
         Convolver()
         {
         /* initialize freq delay line */
         fdl.Init();
         
         
         /* copy IR coefficients to cmplx_ir buffer */
         memcpy(cmplx_ir, IR, sizeof(float32_t *) * IR_SIZE);
             
        /* split the impulse response into subfilters */
        int cmplx_subfilter_size = 0;
        for (auto i = 0; i < NUM_SUBFILTERS; i++)
        {
          /*  if not the the last subfilter */
          if (i < NUM_SUBFILTERS - 1)
          {
            cmplx_subfilter_size = FFT_SIZE;
          }
          else
          {
            /* does the last subfilter fill the BLOCK_SIZE array? */
            int remainder = 2 * (IR_SIZE % BLOCK_SIZE);
            cmplx_subfilter_size = remainder == 0 ? FFT_SIZE : remainder;
          }

        /* copy to subfilter multidimensional array */
        arm_copy_f32(&cmplx_ir[i * FFT_SIZE], cmplx_subfilters[i], cmplx_subfilter_size);

        /* fft of subfilter */
        arm_cfft_f32(&arm_cfft_sR_f32_len128, cmplx_subfilters[i], 0, 1);
     }
   }


   /* process a block of data */
   void processBlock(float32_t *pSrc, float32_t *pDst)
   {
 
     /* create complex array from pSrc */
     arm_fill_f32(0.0, cmplx_src, 2 * FFT_SIZE);
     for (auto i = 0; i < BLOCK_SIZE; i++)
     {
       cmplx_src[2 * i] = *(pSrc++);
       cmplx_src[2 * i + 1] = 0.0;
     }
 
     /* fft of cmplx_src */
     arm_cfft_f32(&arm_cfft_sR_f32_len128, cmplx_src, 0, 1);
 
     /* copy input block to fdl slot*/
     slot s;
     memcpy(s.spectra, cmplx_src, sizeof(float32_t *) * (2 * FFT_SIZE));
     fdl.Write(s);
 
     /* shift data */
     fdl.Read();
 
     /* access data in the freq delay line */
     slot one[NUM_SUBFILTERS];
     fdl.ImmediateRead(one, NUM_SUBFILTERS);
 
     arm_fill_f32(0.0, spectral_additions, 2 * FFT_SIZE);
     for (auto i = 0; i < NUM_SUBFILTERS; i++)
     {
       arm_fill_f32(0.0, SRCxSUBFILTER, 2 * FFT_SIZE);
       /* sub filter spectra are pairwisely multiplied with freq delay line */
       arm_cmplx_mult_cmplx_f32(one[NUM_SUBFILTERS - i].spectra, cmplx_subfilters[i], SRCxSUBFILTER, FFT_SIZE);
 
       /* the results are accumulated in the frequency-domain */
       arm_add_f32(spectral_additions, SRCxSUBFILTER, spectral_additions, 2 * FFT_SIZE);
     }
 
     /* ifft of spectral additions */
     arm_cfft_f32(&arm_cfft_sR_f32_len128, spectral_additions, 1, 1);
 
     /* real from complex output */
     arm_fill_f32(0.0, real, NEW_BLOCK_SIZE);
     for (auto i = 0; i < NEW_BLOCK_SIZE; i++)
     {
       real[i] = spectral_additions[2 * i];
     }
 
     /* handle overlap */
     for (auto i = BLOCK_SIZE; i < NEW_BLOCK_SIZE; i++)
     {
       real[i - BLOCK_SIZE] += olap[i - BLOCK_SIZE];
       olap[i - BLOCK_SIZE] = real[i];
     }
 
     /* copy samples to ouput buffer */
     memcpy(pDst, real, sizeof(float32_t *) * BLOCK_SIZE);
   }
        
        private:
          float32_t cmplx_src[2 * FFT_SIZE];
          float32_t cmplx_ir[2 * IR_SIZE];
          float32_t SRCxSUBFILTER[2 * FFT_SIZE];
          float32_t cmplx_subfilters[NUM_SUBFILTERS][2 * FFT_SIZE];
          float32_t spectral_additions[2 * FFT_SIZE];
          float32_t olap[OVERLAP];
          float32_t real[NEW_BLOCK_SIZE];
          daisy::RingBuffer<slot, NUM_SUBFILTERS> fdl;
          float32_t IR[2 * IR_SIZE] ={${cmplxIR.join(',')}};

    };
    #else // USE_ARM_DSP
    #endif // USE_ARM_DSP`

    fs.writeFileSync(
        '../../oopsy-terrarium-support/source/build_My_Effect_terrarium/upola-convolution.h',
        ir_template);


    const build_path =
        '../../oopsy-terrarium-support/source/build_My_Effect_terrarium/';
    try {
      if (platform == 'win32') {
        execSync('del /Q build', {cwd: build_path, stdio: 'pipe'})
        execSync('make', {cwd: build_path, stdio: 'pipe'})
        execSync('make program-dfu', {cwd: build_path, stdio: 'pipe'})
      } else if (platform == 'darwin') {
        execSync('make clean', {cwd: build_path, stdio: 'pipe'})
        execSync(
            'export PATH=$PATH:/usr/local/bin && make',
            {cwd: build_path, stdio: 'pipe'})
        console.log(
            execSync('export PATH=$PATH:/usr/local/bin && make program-dfu', {
              cwd: build_path,
              stdio: 'inherit'
            }).toString())
      }

    } catch (e) {
      console.error(e);
    }

    console.log(`Upload ${filename} complete.`);
  });
});
