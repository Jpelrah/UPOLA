{
	"patcher" : 	{
		"fileversion" : 1,
		"appversion" : 		{
			"major" : 8,
			"minor" : 2,
			"revision" : 1,
			"architecture" : "x64",
			"modernui" : 1
		}
,
		"classnamespace" : "box",
		"rect" : [ 34.0, 76.0, 1069.0, 707.0 ],
		"bglocked" : 0,
		"openinpresentation" : 0,
		"default_fontsize" : 12.0,
		"default_fontface" : 0,
		"default_fontname" : "Arial",
		"gridonopen" : 1,
		"gridsize" : [ 15.0, 15.0 ],
		"gridsnaponopen" : 1,
		"objectsnaponopen" : 1,
		"statusbarvisible" : 2,
		"toolbarvisible" : 1,
		"lefttoolbarpinned" : 0,
		"toptoolbarpinned" : 0,
		"righttoolbarpinned" : 0,
		"bottomtoolbarpinned" : 0,
		"toolbars_unpinned_last_save" : 0,
		"tallnewobj" : 0,
		"boxanimatetime" : 200,
		"enablehscroll" : 1,
		"enablevscroll" : 1,
		"devicewidth" : 0.0,
		"description" : "",
		"digest" : "",
		"tags" : "",
		"style" : "",
		"subpatcher_template" : "",
		"assistshowspatchername" : 0,
		"boxes" : [ 			{
				"box" : 				{
					"fontface" : 1,
					"fontsize" : 12.0,
					"id" : "obj-4",
					"maxclass" : "comment",
					"numinlets" : 0,
					"numoutlets" : 0,
					"patching_rect" : [ 137.699999999999989, 50.607142857142861, 184.800000000000011, 20.0 ],
					"suppressinlet" : 1,
					"text" : "(Double-click To Upload)",
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"bgmode" : 0,
					"border" : 0,
					"clickthrough" : 0,
					"enablehscroll" : 0,
					"enablevscroll" : 0,
					"id" : "obj-10",
					"lockeddragscroll" : 0,
					"lockedsize" : 0,
					"maxclass" : "bpatcher",
					"name" : "n4m.monitor.maxpat",
					"numinlets" : 1,
					"numoutlets" : 1,
					"offset" : [ 0.0, 0.0 ],
					"outlettype" : [ "bang" ],
					"patching_rect" : [ 447.199999999999989, 404.0, 400.0, 220.0 ],
					"viewvisibility" : 1
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-9",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 432.199999999999989, 320.0, 166.0, 22.0 ],
					"text" : "script npm install fs node-wav"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-8",
					"maxclass" : "message",
					"numinlets" : 2,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 366.199999999999989, 320.0, 64.0, 22.0 ],
					"text" : "script start"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-6",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 263.199999999999989, 320.0, 101.0, 22.0 ],
					"text" : "prepend filename"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-5",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 2,
					"outlettype" : [ "", "" ],
					"patching_rect" : [ 263.199999999999989, 364.0, 203.0, 22.0 ],
					"saved_object_attributes" : 					{
						"autostart" : 1,
						"defer" : 0,
						"node_bin_path" : "",
						"npm_bin_path" : "",
						"watch" : 0
					}
,
					"text" : "node.script uploadIR.js @autostart 1"
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"fontsize" : 16.0,
					"id" : "obj-3",
					"maxclass" : "comment",
					"numinlets" : 1,
					"numoutlets" : 0,
					"patching_rect" : [ 148.5, 21.607142857142861, 163.199999999999989, 25.0 ],
					"text" : "Impulse Responses",
					"textjustification" : 1
				}

			}
, 			{
				"box" : 				{
					"fontface" : 1,
					"fontname" : "Arial",
					"fontsize" : 16.0,
					"id" : "obj-2",
					"items" : [ "01_EBS_PL_410_by_Shift_Line.wav", ",", "02_Hartke_XL410_by_Shift_Line.wav", ",", "03_TC_Electronic_BC410_by_Shift_Line.wav", ",", "04_Ampeg_Heritage_B15_by_Shift_Line.wav", ",", "05_Sunn_200s_by_Shift_Line.wav", ",", "06_Mesa_RR215_by_Shift_Line.wav", ",", "07_Ampeg_SVT-810E_by_Shift_Line.wav", ",", "08_ElectroVoice_EVM12L_by_Shift_Line.wav", ",", "09_Orange_PPC212_bass_edition_by_Shift_Line.wav", ",", "10_Celestion_V30_bass_edition_by_Shift_Line.wav", ",", "11_Sunn_200s_Olympic_MkIIIS_edition2.wav", ",", "12_Ampeg_SVT-810E_Olympic_MkIIIS_edition2.wav", ",", "Low-pass-0-50Hz.wav", ",", "Low-pass-1500-2000kHz.wav", ",", "Low-pass-2000-4000kHz.wav", ",", "Low-pass-2400-2500kHz.wav", ",", "Low-pass-250-300Hz.wav", ",", "Low-pass-2500-2900kHz.wav", ",", "Low-pass-2800-3000kHz.wav", ",", "Low-pass-50-80Hz.wav", ",", "Low-pass-500-2000kHz.wav", ",", "Low-pass-700-2000kHz.wav", ",", "Low-pass-80-105Hz.wav", ",", "Low-pass-80-108Hz.wav", ",", "Low-pass-80-110Hz.wav", ",", "Low-pass-900-2000kHz.wav", ",", "Low-pass-920-2000kHz.wav", ",", "Low-pass-930-2000kHz.wav", ",", "Low-pass-940-2000kHz.wav", ",", "Low-pass-945-1000kHz.wav", ",", "Rectifier-SM57-Impact.wav" ],
					"maxclass" : "chooser",
					"multiselect" : 0,
					"numinlets" : 1,
					"numoutlets" : 6,
					"outlettype" : [ "", "", "", "", "", "" ],
					"parameter_enable" : 0,
					"patching_rect" : [ 8.599999999999994, 72.607142857142861, 443.0, 235.000000000000028 ],
					"textjustification" : 0,
					"varname" : "IRChooser"
				}

			}
, 			{
				"box" : 				{
					"id" : "obj-1",
					"maxclass" : "newobj",
					"numinlets" : 1,
					"numoutlets" : 1,
					"outlettype" : [ "" ],
					"patching_rect" : [ 256.600000000000023, 72.607142857142861, 102.0, 22.0 ],
					"saved_object_attributes" : 					{
						"filename" : "chooserSetup",
						"parameter_enable" : 0
					}
,
					"text" : "js chooserSetup"
				}

			}
 ],
		"lines" : [ 			{
				"patchline" : 				{
					"destination" : [ "obj-6", 0 ],
					"source" : [ "obj-2", 3 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-10", 0 ],
					"source" : [ "obj-5", 1 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"source" : [ "obj-6", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"midpoints" : [ 375.699999999999989, 352.5, 272.699999999999989, 352.5 ],
					"source" : [ "obj-8", 0 ]
				}

			}
, 			{
				"patchline" : 				{
					"destination" : [ "obj-5", 0 ],
					"midpoints" : [ 441.699999999999989, 352.5, 272.699999999999989, 352.5 ],
					"source" : [ "obj-9", 0 ]
				}

			}
 ],
		"dependency_cache" : [ 			{
				"name" : "chooserSetup.js",
				"bootpath" : "~/Desktop/UPOLA/IRLoader/code",
				"patcherrelativepath" : "../code",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "uploadIR.js",
				"bootpath" : "~/Desktop/UPOLA/IRLoader/code",
				"patcherrelativepath" : "../code",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "n4m.monitor.maxpat",
				"bootpath" : "C74:/packages/Node for Max/patchers/debug-monitor",
				"type" : "JSON",
				"implicit" : 1
			}
, 			{
				"name" : "resize_n4m_monitor_patcher.js",
				"bootpath" : "C74:/packages/Node for Max/patchers/debug-monitor",
				"type" : "TEXT",
				"implicit" : 1
			}
, 			{
				"name" : "fit_jweb_to_bounds.js",
				"bootpath" : "C74:/packages/Node for Max/patchers/debug-monitor",
				"type" : "TEXT",
				"implicit" : 1
			}
 ],
		"autosave" : 0
	}

}
