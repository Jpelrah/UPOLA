{
	"name" : "IRLoader",
	"version" : 1,
	"creationdate" : 3724262270,
	"modificationdate" : 3724263311,
	"viewrect" : [ 944.0, 178.0, 300.0, 500.0 ],
	"autoorganize" : 0,
	"hideprojectwindow" : 0,
	"showdependencies" : 1,
	"autolocalize" : 0,
	"contents" : 	{
		"patchers" : 		{
			"main.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1,
				"toplevel" : 1
			}

		}
,
		"code" : 		{
			"chooserSetup.js" : 			{
				"kind" : "javascript",
				"local" : 1,
				"singleton" : 				{
					"bootpath" : "~/Desktop/UPOLA/IRLoader/code",
					"projectrelativepath" : "./code"
				}

			}
,
			"uploadIR.js" : 			{
				"kind" : "javascript",
				"local" : 1
			}

		}

	}
,
	"layout" : 	{

	}
,
	"searchpath" : 	{

	}
,
	"detailsvisible" : 0,
	"amxdtype" : 0,
	"readonly" : 0,
	"devpathtype" : 0,
	"devpath" : ".",
	"sortmode" : 0,
	"viewmode" : 0
}
